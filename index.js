// Run some jQuery on a html fragment
var fs = require('fs'),
    jsdom = require("jsdom"),
    toMarkdown = require('to-markdown').toMarkdown,
    file = fs.readFileSync('./template.html', "utf8");

//console.log(file);

jsdom.env(
  file,
  ["http://code.jquery.com/jquery.js"],
  function (errors, window) {
    var process = true,
        content;

    window.$("style").remove();
    window.$("div[id^='crayon-']").remove('class');
    window.$("div.crayon-toolbar,div.crayon-info,div.crayon-main").remove();
    window.$("span").removeAttr('class');
    window.$("span,div,textarea").removeAttr('style');
    window.$("span").removeAttr('id');
    window.$("div[id^='crayon-']").remove('class');


    content = window.$('.entry-content').html();

    //console.log(content);

    fs.writeFile("output.md", process?toMarkdown(content):content, function(err) {
        if(err) {
            console.log(err);
        } else {
            console.log("The file was saved!");
        }
    }); 
  }
);